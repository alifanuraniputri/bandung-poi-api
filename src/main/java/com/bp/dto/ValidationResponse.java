package com.bp.dto;

/**
 * Created by Alifa on 6/23/2015.
 */
public class ValidationResponse {
    private int code;
    private String message;
    private long id;
    private String name;

    public ValidationResponse(int code, String message, long id, String name) {
        this.code = code;
        this.message = message;
        this.id = id;
        this.name = name;
    }

    public ValidationResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
