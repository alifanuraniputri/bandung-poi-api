package com.bp.controller;

import com.bp.dao.LocationDao;
import com.bp.dao.RatingDao;
import com.bp.dao.UserDao;
import com.bp.entity.CheckIn;
import com.bp.entity.Location;
import com.bp.entity.Rating;
import com.bp.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */

@Controller
@RequestMapping(value="/rating")
public class RatingController {
    private static Logger log = LoggerFactory.getLogger(RatingController.class);

    @Inject
    private RatingDao ratingDAO;
    @Inject
    private LocationDao locationDao;
    @Inject
    private UserDao userDao;

    @RequestMapping(method= RequestMethod.GET, value="/{ratingId}")
    public @ResponseBody
    Rating getRating (@PathVariable(value="ratingId") long ratingId, Model model) {

        log.info("Searching for rating with id = " + ratingId);

        Rating rating = ratingDAO.getRatingById(ratingId);
        return rating;
    }

    @RequestMapping(method=RequestMethod.GET, value="/list")
    public @ResponseBody
    List<Rating> getAllRating (Model model) {

        log.info("Searching for all rating");

        List<Rating> ratings = ratingDAO.getAllRating();

        return ratings;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public @ResponseBody
    Rating addRating( @RequestParam(value = "userId") Long userId, @RequestParam(value = "locationId") Long locationId, @RequestParam(value = "criteria1") Integer criteria1, @RequestParam(value = "criteria2") Integer criteria2 ,
                      @RequestParam(value = "criteria3") Integer criteria3, @RequestParam(value = "criteria4") Integer criteria4,
                      @RequestParam(value = "criteria5") Integer criteria5, @RequestParam(value = "overall") Integer overall,
                      Model model) {
        log.info("new rating user : "+ userId + "location : "+locationId );

        Rating rating = new Rating();
        Location location = locationDao.getLocationById(locationId);
        User user = userDao.getUserById(userId);
        rating.setLocation(location);
        rating.setUser(user);
        rating.setCriteria1(criteria1);
        rating.setCriteria2(criteria2);
        rating.setCriteria3(criteria3);
        rating.setCriteria4(criteria4);
        rating.setCriteria5(criteria5);
        rating.setOverall(overall);
        rating = ratingDAO.addNewRating(rating);

        return rating;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    Rating deleteRating(@RequestParam(value = "id") long id, Model model) {
        log.info("delete rating id = " + id);

        Rating rating = ratingDAO.getRatingById(id);
        if (rating != null) {
            rating.setStatus(0);
            rating = ratingDAO.updateRating(rating);
        }
        return rating;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/listByUser")
    public @ResponseBody List<Rating> getRatingByUserId (@RequestParam(value = "userId") Long userId, Model model) {

        log.info("Searching for check in with user_id  = " + userId);

        List<Rating> checkIns = ratingDAO.getRatingByUserId(userId);

        return checkIns;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/listByLocation")
    public @ResponseBody List<Rating> getRatingByLocationId (@RequestParam(value = "locationId") Long locationId, Model model) {

        log.info("Searching for check in with location_id  = " + locationId);

        List<Rating> checkIns = ratingDAO.getRatingByLocationId(locationId);

        return checkIns;
    }

}
