package com.bp.controller;

import com.bp.dao.CheckInDao;
import com.bp.dao.LocationDao;
import com.bp.dao.UserDao;
import com.bp.entity.CheckIn;
import com.bp.entity.Location;
import com.bp.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */

@Controller
@RequestMapping(value="/checkin")
public class CheckInController {
    private static Logger log = LoggerFactory.getLogger(CheckInController.class);

    @Inject
    private CheckInDao checkinDAO;
    @Inject
    private LocationDao locationDao;
    @Inject
    private UserDao userDao;

    @RequestMapping(method= RequestMethod.GET, value="/{checkinId}")
    public @ResponseBody
    CheckIn getCheckIn (@PathVariable(value="checkinId") long checkinId, Model model) {

        log.info("Searching for checkin with id = " + checkinId);

        CheckIn checkin = checkinDAO.getCheckInById(checkinId);
        return checkin;
    }

    @RequestMapping(method=RequestMethod.GET, value="/list")
    public @ResponseBody
    List<CheckIn> getAllCheckIn (Model model) {

        log.info("Searching for all checkin");

        List<CheckIn> checkins = checkinDAO.getAllCheckIn();

        return checkins;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public @ResponseBody
    CheckIn addCheckIn( @RequestParam(value = "userId") Long userId, @RequestParam(value = "locationId") Long locationId, Model model) {
        log.info("new checkin user : "+ userId + "location : "+locationId );

        CheckIn checkin = new CheckIn();
        Location location = locationDao.getLocationById(locationId);
        User user = userDao.getUserById(userId);
        checkin.setLocation(location);
        checkin.setUser(user);
        checkin = checkinDAO.addNewCheckIn(checkin);

        return checkin;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    CheckIn deleteCheckIn(@RequestParam(value = "id") long id, Model model) {
        log.info("delete checkin id = " + id);

        CheckIn checkin = checkinDAO.getCheckInById(id);
        if (checkin != null) {
            checkin.setStatus(0);
            checkin = checkinDAO.updateCheckIn(checkin);
        }
        return checkin;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/listByUser")
    public @ResponseBody List<CheckIn> getCheckInByUserId (@RequestParam(value = "userId") Long userId, Model model) {

        log.info("Searching for check in with user_id  = " + userId);

        List<CheckIn> checkIns = checkinDAO.getCheckInByUserId(userId);

        return checkIns;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/listByLocation")
    public @ResponseBody List<CheckIn> getCheckInByLocationId (@RequestParam(value = "locationId") Long locationId, Model model) {

        log.info("Searching for check in with location_id  = " + locationId);

        List<CheckIn> checkIns = checkinDAO.getCheckInByLocationId(locationId);

        return checkIns;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/countByLocation")
    public @ResponseBody int countCheckInByLocationId (@RequestParam(value = "locationId") Long locationId, Model model) {

        log.info("Searching for check in with location_id  = " + locationId);

        List<CheckIn> checkIns = checkinDAO.getCheckInByLocationId(locationId);

        return checkIns.size();
    }
}
