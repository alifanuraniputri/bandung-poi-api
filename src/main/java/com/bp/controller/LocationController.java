package com.bp.controller;

import com.bp.dao.CategoryDao;
import com.bp.dao.LocationDao;
import com.bp.entity.Category;
import com.bp.entity.Issue;
import com.bp.entity.Location;
import com.bp.entity.Rank;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.*;

/**
 * Created by Alifa on 3/27/2015.
 */

@Controller
@RequestMapping(value="/location")
public class LocationController {
    private static Logger log = LoggerFactory.getLogger(LocationController.class);

    @Inject
    private LocationDao locationDAO;
    @Inject
    private CategoryDao categoryDao;

    @RequestMapping(method= RequestMethod.GET, value="/{locationId}")
    public @ResponseBody
    Location getLocation (@PathVariable(value="locationId") long locationId, Model model) {

        log.info("Searching for location with id = " + locationId);

        Location location = locationDAO.getLocationById(locationId);
        return location;
    }

    @RequestMapping(method= RequestMethod.GET, value="rank/{locationId}")
    public @ResponseBody
    Rank getRank (@PathVariable(value="locationId") long locationId, Model model) {

        log.info("Searching rank for location with id = " + locationId);

        Location location = locationDAO.getLocationById(locationId);

        List<Location> locations = locationDAO.getLocationByCategoryId(location.getCategory().getId());
        Rank rank = new Rank();
        rank.setTotal(locations.size());

        Collections.sort(locations, new Comparator<Location>() {

            public int compare(Location o1, Location o2) {
                return o2.getPopularity().compareTo(o1.getPopularity());
            }
        });

        for (int i=0; i< locations.size(); i++) {
            if (locations.get(i).getId() == locationId) {
                rank.setPopularityRank(i+1);
                break;
            }
        }

        Collections.sort(locations, new Comparator<Location>() {

            public int compare(Location o1, Location o2) {
                return o2.getRate().compareTo(o1.getRate());
            }
        });

        for (int i=0; i< locations.size(); i++) {
            if (locations.get(i).getId() == locationId) {
                rank.setRateRank(i+1);
                break;
            }
        }

        return rank;
    }

    @RequestMapping(method=RequestMethod.GET, value="/popular")
    public @ResponseBody
    List<Location> getPopular (@RequestParam(value = "park") Boolean park, @RequestParam(value = "hospital") Boolean hospital,
                               @RequestParam(value = "mosque") Boolean mosque,Model model) {

        log.info("Searching for all location");

        List<Location> locations = locationDAO.getAllLocation();

        if (!park) {
            Iterator<Location> it = locations.iterator();
            while (it.hasNext()) {
                Location location = it.next();
                if (location.getCategory().getId()==300) {
                    it.remove();
                }
            }
        }
        if (!hospital) {
            Iterator<Location> it = locations.iterator();
            while (it.hasNext()) {
                Location location = it.next();
                if (location.getCategory().getId()==3) {
                    it.remove();
                }
            }
        }
        if (!mosque) {
            Iterator<Location> it = locations.iterator();
            while (it.hasNext()) {
                Location location = it.next();
                if (location.getCategory().getId()==2) {
                    it.remove();
                }
            }
        }
        Collections.sort(locations, new Comparator<Location>() {

            public int compare(Location o1, Location o2) {
                return o2.getPopularity().compareTo(o1.getPopularity());
            }
        });

        return locations;
    }

    @RequestMapping(method=RequestMethod.GET, value="/bestRated")
    public @ResponseBody
    List<Location> getBestRated (@RequestParam(value = "park") Boolean park, @RequestParam(value = "hospital") Boolean hospital,
                                 @RequestParam(value = "mosque") Boolean mosque, Model model) {

        log.info("Searching for all location");

        List<Location> locations = locationDAO.getAllLocation();

        if (!park) {
            Iterator<Location> it = locations.iterator();
            while (it.hasNext()) {
                Location location = it.next();
                if (location.getCategory().getId()==300) {
                    it.remove();
                }
            }
        }
        if (!hospital) {
            Iterator<Location> it = locations.iterator();
            while (it.hasNext()) {
                Location location = it.next();
                if (location.getCategory().getId()==3) {
                    it.remove();
                }
            }
        }
        if (!mosque) {
            Iterator<Location> it = locations.iterator();
            while (it.hasNext()) {
                Location location = it.next();
                if (location.getCategory().getId()==2) {
                    it.remove();
                }
            }
        }

        Collections.sort(locations, new Comparator<Location>() {

            public int compare(Location o1, Location o2) {
                return o2.getRate().compareTo(o1.getRate());
            }
        });

        return locations;
    }

    public static Map sortByValue(Map unsortMap) {
        List list = new LinkedList(unsortMap.entrySet());

        Comparator original = new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        };

        Collections.sort(list,new ReverseComparator(original));

        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    @RequestMapping(method=RequestMethod.GET, value="/issue")
    public @ResponseBody
    List<Issue> getIssue (Model model) {

        log.info("Searching for all location");

        List<Location> locations = locationDAO.getAllLocation();

        HashMap<String,Integer> issues = new HashMap<String,Integer>();
        for (int i=0; i<locations.size(); i++) {
            if (!locations.get(i).getIssue1().equals(""))
            //issue1
                if (issues.containsKey(locations.get(i).getIssue1())) {
                    issues.put(locations.get(i).getIssue1(),issues.get(locations.get(i).getIssue1())+locations.get(i).getHits1());
                } else {
                    issues.put(locations.get(i).getIssue1(), locations.get(i).getHits1());
                }
            if (!locations.get(i).getIssue2().equals(""))
            //issue2
                if (issues.containsKey(locations.get(i).getIssue2())) {
                    issues.put(locations.get(i).getIssue2(),issues.get(locations.get(i).getIssue2())+locations.get(i).getHits2());
                } else {
                    issues.put(locations.get(i).getIssue2(),locations.get(i).getHits2());
                }
            if (!locations.get(i).getIssue3().equals(""))
            //issue3
                if (issues.containsKey(locations.get(i).getIssue3())) {
                    issues.put(locations.get(i).getIssue3(),issues.get(locations.get(i).getIssue3())+locations.get(i).getHits3());
                } else {
                    issues.put(locations.get(i).getIssue3(),locations.get(i).getHits3());
                }
        }

        Map<String,Integer> sortedMap = sortByValue(issues);

        List<Issue> issuesSorted = new ArrayList<>();

        for (String key : sortedMap.keySet()) {
            issuesSorted.add(new Issue(key,sortedMap.get(key)));
        }



        return issuesSorted;
    }


    @RequestMapping(method=RequestMethod.GET, value="/list")
    public @ResponseBody
    List<Location> getAllLocation (Model model) {

        log.info("Searching for all location");

        List<Location> locations = locationDAO.getAllLocation();

        return locations;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public @ResponseBody
    Location addLocation(@RequestParam(value = "categoryId") Long categoryId, @RequestParam(value = "name") String name , @RequestParam(value = "description") String description,
                         @RequestParam(value = "lat") Double lat, @RequestParam(value = "lng") Double lng, Model model) {


        Location location = new Location();
        Category category = categoryDao.getCategoryById(categoryId);
        if (category!=null) {
            log.info("new location = " + name + " category = "+ categoryId);
            location.setCategory(category);
            location.setName(name);
            location.setDescription(description);
            location.setLat(lat);
            location.setLng(lng);
            location.setCriteria1(0.0);
            location.setCriteria2(0.0);
            location.setCriteria3(0.0);
            location.setCriteria4(0.0);
            location.setCriteria5(0.0);
            location.setPopularity(0.0);
            location.setRate(0.0);
            location.setHits1(0);
            location.setHits2(0);
            location.setHits3(0);
            location = locationDAO.addNewLocation(location);
        }
        return location;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public @ResponseBody
    Location updateLocation(@RequestParam(value = "id") Long id, @RequestParam(value = "categoryId") Long categoryId, @RequestParam(value = "name") String name , @RequestParam(value = "description") String description,
                            @RequestParam(value = "lat") Double lat, @RequestParam(value = "lng") Double lng, @RequestParam(value = "foursquareId") String foursquareId,
                            Model model) {

        Location location = locationDAO.getLocationById(id);

        log.info("update location id = " + id );

        if (location != null) {
            if (name!=null)
                location.setName(name);
            if (description!=null)
                location.setDescription(description);
            if (lat!=null)
                location.setLat(lat);
            if (lng!=null)
                location.setLng(lng);
            if (categoryId!=null) {
                Category category = categoryDao.getCategoryById(categoryId);
                location.setCategory(category);
            }
            if (foursquareId!=null) {
                location.setFoursquareId(foursquareId);
            }
            location = locationDAO.updateLocation(location);
        }
        return location;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    Location deleteLocation(@RequestParam(value = "id") long id, Model model) {
        log.info("delete location id = " + id);

        Location location = locationDAO.getLocationById(id);
        if (location != null) {
            location.setStatus(0);
            location = locationDAO.updateLocation(location);
        }
        return location;
    }

    @RequestMapping(method=RequestMethod.GET, value = "/listByCategory")
    public @ResponseBody List<Location> getLocationByCategory (@RequestParam(value = "categoryId") Long categoryId, Model model) {

        log.info("Searching for location with category_id  = " + categoryId);

        List<Location> locations = locationDAO.getLocationByCategoryId(categoryId);

        return locations;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updatePopularity")
    public @ResponseBody
    Location updateLocation(@RequestParam(value = "id") Long id, @RequestParam(value = "popularity") Double popularity, Model model) {

        Location location = locationDAO.getLocationById(id);

        log.info("update popularity id = " + id );

        if (location != null) {
            if (popularity!=null)
                location.setPopularity(popularity);
            location = locationDAO.updateLocation(location);
        }
        return location;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updateRate")
    public @ResponseBody
    Location updateRate(@RequestParam(value = "id") Long id, @RequestParam(value = "rate") Double rate, Model model) {

        Location location = locationDAO.getLocationById(id);

        log.info("update popularity id = " + id );

        if (location != null) {
            if (rate!=null)
                location.setRate(rate);
            location = locationDAO.updateLocation(location);
        }
        return location;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updateCriterias")
    public @ResponseBody
    Location updateCriterias(@RequestParam(value = "id") Long id, @RequestParam(value = "criteria1") Double criteria1,
                        @RequestParam(value = "criteria2") Double criteria2, @RequestParam(value = "criteria3") Double criteria3,
                        @RequestParam(value = "criteria4") Double criteria4, @RequestParam(value = "criteria5") Double criteria5,
                        Model model) {

        Location location = locationDAO.getLocationById(id);

        log.info("update criterias id = " + id );

        if (location != null) {
            if (criteria1!=null)
                location.setCriteria1(criteria1);
            if (criteria2!=null)
                location.setCriteria2(criteria2);
            if (criteria3!=null)
                location.setCriteria3(criteria3);
            if (criteria4!=null)
                location.setCriteria4(criteria4);
            if (criteria5!=null)
                location.setCriteria5(criteria5);
            location = locationDAO.updateLocation(location);
        }
        return location;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updateIssues")
    public @ResponseBody
    Location updateIssues(@RequestParam(value = "id") Long id, @RequestParam(value = "issue1") String issue1,
                             @RequestParam(value = "issue2") String issue2, @RequestParam(value = "issue3") String issue3,
                             @RequestParam(value = "hits1") Integer hits1, @RequestParam(value = "hits2") Integer hits2,
                             @RequestParam(value = "hits3") Integer hits3,
                             Model model) {

        Location location = locationDAO.getLocationById(id);

        log.info("update issues id = " + id );

        if (location != null) {
            if (issue1!=null)
                location.setIssue1(issue1);
            if (issue2!=null)
                location.setIssue2(issue2);
            if (issue3!=null)
                location.setIssue3(issue3);
            if (hits1!=null)
                location.setHits1(hits1);
            if (hits2!=null)
                location.setHits2(hits2);
            if (hits3!=null)
                location.setHits3(hits3);
            location = locationDAO.updateLocation(location);
        }
        return location;
    }
}
