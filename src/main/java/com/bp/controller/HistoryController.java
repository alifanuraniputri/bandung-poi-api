package com.bp.controller;

import com.bp.dao.HistoryDao;
import com.bp.dao.UserDao;
import com.bp.entity.History;
import com.bp.entity.Rating;
import com.bp.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Alifa on 6/10/2015.
 */
@Controller
@RequestMapping(value="/history")
public class HistoryController {

    private static Logger log = LoggerFactory.getLogger(HistoryController.class);

    @Inject
    private UserDao userDAO;

    @Inject
    private HistoryDao historyDao;

    @RequestMapping(method=RequestMethod.GET, value="/user/{userId}")
    public @ResponseBody
    List<History> getRatingByUserId (@RequestParam(value = "user_id") Long user_id, Model model) {

        log.info("Searching for check in with user_id  = " + user_id);

        List<History> histories = historyDao.getHistoryByUserId(user_id);

        return histories;
    }
}
