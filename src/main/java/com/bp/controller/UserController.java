package com.bp.controller;

import com.bp.common.SHA1;
import com.bp.dao.UserDao;
import com.bp.dto.UserDto;
import com.bp.dto.ValidationResponse;
import com.bp.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.inject.Inject;
import java.security.SecureRandom;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by Alifa on 3/12/2015.
 */
@Controller
@RequestMapping(value="/user")
public class UserController {

    private static Logger log = LoggerFactory.getLogger(UserController.class);

    @Inject
    private UserDao userDAO;

    @RequestMapping(method=RequestMethod.GET, value="/{userId}")
    public @ResponseBody User getUser (@PathVariable(value="userId") long userId, Model model) {

        log.info("Searching for user with id = " + userId);

        User user = userDAO.getUserById(userId);
        return user;
    }

    @RequestMapping(method=RequestMethod.GET, value="/list")
    public @ResponseBody List<User> getAllUser (Model model) {

        log.info("Searching for all user");

        List<User> users = userDAO.getAllUser();

        return users;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public @ResponseBody
    UserDto addUser(@RequestParam(value = "username") String username , @RequestParam(value = "name") String name,
                 @RequestParam(value = "psswd") String psswd, Model model) {
        log.info("new user = " + username + " name = " + name);

        SecureRandom random = new SecureRandom();

        User user = new User();
        user.setUsername(username);
        user.setName(name);
        String userSalt = new BigInteger(130, random).toString(32);
        user.setSalt(userSalt);
        SHA1 sha1 = new SHA1(psswd+user.getSalt());
        user.setHash(sha1.hash());

        user = userDAO.addNewUser(user);
        UserDto dto = new UserDto(user.getId(),user.getName(),user.getUsername());
        return dto;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public @ResponseBody
    User updateUser(@RequestParam(value = "id") long id, @RequestParam(value = "username") String username , @RequestParam(value = "name") String name, Model model) {
        log.info("update user id = " + id + " username " + username + " name = " + name);

        User user = userDAO.getUserById(id);
        if (user != null) {
            if (!username.equals(""))
                user.setUsername(username);
            if (!name.equals(""))
                user.setName(name);
            user = userDAO.updateUser(user);
        }
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    User deleteUser(@RequestParam(value = "id") long id, Model model) {
        log.info("delete user id = " + id);

        User user = userDAO.getUserById(id);
        if (user != null) {
            user.setStatus(0);
            user = userDAO.updateUser(user);
        }
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/validate")
    public @ResponseBody
    ValidationResponse validateUser(@RequestParam(value = "username") String username, @RequestParam(value = "psswd") String psswd, Model model) {
        log.info("validate user with id = " + username );

        User user = userDAO.getUserByUsername(username);
        if (user != null) {
            SHA1 sha1 = new SHA1(psswd+user.getSalt());
            if (user.getHash().equals(sha1.hash())) {
                return new ValidationResponse(1, "valid", user.getId(), user.getName());
            }
        }
        return new ValidationResponse(0, "invalid");
    }

}
