package com.bp.controller;

import com.bp.Exception.CategoryException;
import com.bp.dao.CategoryDao;
import com.bp.entity.Category;
import com.bp.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
@Controller
@RequestMapping(value="/category")
public class CategoryController {

    private static Logger log = LoggerFactory.getLogger(CategoryController.class);

    @Inject
    private CategoryDao categoryDAO;

    @ExceptionHandler(CategoryException.class)
    public ResponseEntity<String> rulesForCustomerNotFound(HttpServletRequest req, Exception e)
    {
        return new ResponseEntity<String>(e.toString(), HttpStatus.NOT_FOUND);

    }

    @RequestMapping(method= RequestMethod.GET, value="/{categoryId}")
    public @ResponseBody
    Category getCategory (@PathVariable(value="categoryId") long categoryId, Model model) throws CategoryException{

        log.info("Searching for category with id = " + categoryId);

        Category category = categoryDAO.getCategoryById(categoryId);
        return category;
    }

    @RequestMapping(method=RequestMethod.GET, value="/list")
    public @ResponseBody
    List<Category> getAllCategory (Model model) throws CategoryException {

        log.info("Searching for all category");

        List<Category> categories = categoryDAO.getAllCategory();

        return categories;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public @ResponseBody
    Category addCategory(@RequestParam(value = "name") String name , @RequestParam(value = "description") String description, Model model)
            throws CategoryException {
        log.info("new cateegory = " + name );

        Category category = new Category();
        category.setName(name);
        category.setDescription(description);
        category = categoryDAO.addNewCategory(category);

        return category;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public @ResponseBody
    Category updateCategory(@RequestParam(value = "id") Long id, @RequestParam(value = "name") String name , @RequestParam(value = "description") String description,
                            Model model) throws CategoryException {
        log.info("update category id = " + id + " name " + name + " description = " + description);

        Category category = categoryDAO.getCategoryById(id);
        if (category != null) {
            if (name!=null)
                category.setName(name);
            if (description!=null)
                category.setDescription(description);
            category = categoryDAO.updateCategory(category);
        }
        return category;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete")
    public @ResponseBody
    Category deleteCategory(@RequestParam(value = "id") Long id, Model model) throws CategoryException {
        log.info("delete category id = " + id);

        Category category = categoryDAO.getCategoryById(id);
        if (category != null) {
            category.setStatus(0);
            category = categoryDAO.updateCategory(category);
        }
        return category;
    }
}
