package com.bp.dao;

import com.bp.entity.CheckIn;

import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
public interface CheckInDao {
    List<CheckIn> getAllCheckIn();
    CheckIn addNewCheckIn(CheckIn checkin);
    CheckIn getCheckInById(Long id);
    CheckIn updateCheckIn(CheckIn checkin);
    List<CheckIn> getCheckInByUserId(Long user_id);
    List<CheckIn> getCheckInByLocationId(Long location_id);
}
