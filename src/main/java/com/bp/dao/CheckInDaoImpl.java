package com.bp.dao;

import com.bp.entity.CheckIn;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
public class CheckInDaoImpl extends JpaDaoSupport implements CheckInDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<CheckIn> getAllCheckIn() {
        return getJpaTemplate().find("select c from CheckIn c");
    }

    @Override
    public CheckIn getCheckInById(Long id) {
        return getJpaTemplate().find(CheckIn.class, id);
    }

    @Override
    @Transactional(readOnly=false)
    public CheckIn addNewCheckIn(CheckIn checkin) {

        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(checkin);
        em.flush();
        em.close();

        return checkin;
    }

    @Override
    @Transactional(readOnly=false)
    public CheckIn updateCheckIn(CheckIn checkin) {
        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(checkin);
        em.flush();
        em.close();

        return checkin;
    }

    @Override
    public List<CheckIn> getCheckInByUserId(Long user_id) {
        return getJpaTemplate().find("select c from CheckIn c where c.user.id=?1", user_id);
    }

    @Override
    public List<CheckIn> getCheckInByLocationId(Long location_id) {
        return getJpaTemplate().find("select c from CheckIn c where c.location.id=?1", location_id);
    }
}
