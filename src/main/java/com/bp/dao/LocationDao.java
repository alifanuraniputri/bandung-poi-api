package com.bp.dao;

import com.bp.entity.Location;

import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
public interface LocationDao {
    List<Location> getAllLocation();
    Location addNewLocation(Location location);
    Location getLocationById(Long id);
    List<Location> getLocationByCategoryId(Long id);
    Location updateLocation(Location location);
}
