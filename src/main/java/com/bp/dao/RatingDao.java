package com.bp.dao;

import com.bp.entity.Rating;

import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
public interface RatingDao {
    List<Rating> getAllRating();
    Rating addNewRating(Rating rating);
    Rating getRatingById(Long id);
    Rating updateRating(Rating rating);
    List<Rating> getRatingByUserId(Long user_id);
    List<Rating> getRatingByLocationId(Long location_id);
}
