package com.bp.dao;

import com.bp.entity.Category;
import com.bp.entity.User;

import java.util.List;

/**
 * Created by Alifa on 3/26/2015.
 */
public interface CategoryDao {
    List<Category> getAllCategory();
    Category addNewCategory(Category category);
    Category getCategoryById(Long id);
    Category updateCategory(Category category);
}
