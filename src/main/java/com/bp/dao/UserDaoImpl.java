package com.bp.dao;

import com.bp.dto.UserDto;
import com.bp.entity.User;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Alifa on 3/12/2015.
 */
public class UserDaoImpl  extends JpaDaoSupport implements UserDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<User > getAllUser() {
        return getJpaTemplate().find("select u from User u");
    }

    @Override
     public User getUserById(Long id) {
        return getJpaTemplate().find(User.class, id);
    }

    @Override
    public User getUserByUsername(String usr) {

       Query query = em.createQuery(
                "SELECT c FROM User c WHERE c.username = :usr");
       query.setParameter("usr", usr);
       User c = (User)query.getSingleResult();
       return c;
    }

    @Override
    @Transactional(readOnly=false)
    public User addNewUser(User user) {

        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(user);
        em.flush();
        em.close();

        return user;
    }

    @Override
    @Transactional(readOnly=false)
    public User updateUser(User user) {

        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(user);
        em.flush();
        em.close();

        return user;
    }
}
