package com.bp.dao;

import com.bp.entity.CheckIn;
import com.bp.entity.History;
import com.bp.entity.Rating;

import java.util.List;

/**
 * Created by Alifa on 6/10/2015.
 */
public interface HistoryDao {
    List<CheckIn> getCheckInByUserId(Long user_id);
    List<Rating> getRatingByUserId(Long user_id);
    List<History> getHistoryByUserId(Long user_id);
}
