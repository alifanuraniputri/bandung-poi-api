package com.bp.dao;

import com.bp.entity.CheckIn;
import com.bp.entity.Rating;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
public class RatingDaoImpl extends JpaDaoSupport implements RatingDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Rating> getAllRating() {
        return getJpaTemplate().find("select r from Rating r");
    }

    @Override
    @Transactional(readOnly=false)
    public Rating addNewRating(Rating rating) {
        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.persist(rating);
        em.flush();
        em.close();

        return rating;
    }

    @Override
    public Rating getRatingById(Long id) {
        return getJpaTemplate().find(Rating.class, id);
    }

    @Override
    @Transactional(readOnly=false)
    public Rating updateRating(Rating rating) {
        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(rating);
        em.flush();
        em.close();

        return rating;
    }

    @Override
    public List<Rating> getRatingByUserId(Long user_id) {
        return getJpaTemplate().find("select r from Rating r where r.user.id=?1", user_id);
    }

    @Override
    public List<Rating> getRatingByLocationId(Long location_id) {
        return getJpaTemplate().find("select r from Rating r where r.location.id=?1", location_id);
    }
}
