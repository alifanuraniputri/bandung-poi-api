package com.bp.dao;

import com.bp.entity.CheckIn;
import com.bp.entity.History;
import com.bp.entity.Rating;
import org.springframework.orm.jpa.support.JpaDaoSupport;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Alifa on 6/10/2015.
 */
public class HistoryDaoImpl  extends JpaDaoSupport implements HistoryDao{

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Rating> getRatingByUserId(Long user_id) {
        return getJpaTemplate().find("select r from Rating r where r.user.id=?1", user_id);
    }

    @Override
    public List<History> getHistoryByUserId(Long user_id) {
        return null;
    }

    @Override
    public List<CheckIn> getCheckInByUserId(Long user_id) {
        return getJpaTemplate().find("select c from CheckIn c where c.user.id=?1", user_id);
    }


}
