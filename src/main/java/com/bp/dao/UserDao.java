package com.bp.dao;

import com.bp.dto.UserDto;
import com.bp.entity.User;

import java.util.List;

/**
 * Created by Alifa on 3/12/2015.
 */
public interface UserDao {
    List<User> getAllUser();
    User getUserById(Long id);
    User getUserByUsername(String usr);
    User addNewUser(User user);
    User updateUser(User user);
}
