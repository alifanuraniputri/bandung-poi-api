package com.bp.dao;

import com.bp.entity.Category;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Alifa on 3/26/2015.
 */
public class CategoryDaoImpl extends JpaDaoSupport implements CategoryDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Category> getAllCategory() {
        return getJpaTemplate().find("select c from Category c");
    }

    @Override
    public Category getCategoryById(Long id) {
        return getJpaTemplate().find(Category.class, id);
    }

    @Override
    @Transactional(readOnly=false)
    public Category addNewCategory(Category category) {

        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(category);
        em.flush();
        em.close();

        return category;
    }

    @Override
    public Category updateCategory(Category category) {
        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(category);
        em.flush();
        em.close();

        return category;
    }
}
