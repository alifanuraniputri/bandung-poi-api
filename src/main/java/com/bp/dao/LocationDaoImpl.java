package com.bp.dao;

import com.bp.entity.Location;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
public class LocationDaoImpl  extends JpaDaoSupport implements LocationDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Location> getAllLocation() {
        return getJpaTemplate().find("select l from Location l");
    }

    @Override
    public Location getLocationById(Long id) {
        return getJpaTemplate().find(Location.class, id);
    }

    @Override
    @Transactional(readOnly=false)
    public Location addNewLocation(Location location) {
        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(location);
        em.flush();
        em.close();

        return location;
    }

    @Override
    @Transactional(readOnly=false)
    public Location updateLocation(Location location) {
        em = EntityManagerFactoryUtils.getTransactionalEntityManager(getJpaTemplate().getEntityManagerFactory());
        em.merge(location);
        em.flush();
        em.close();

        return location;
    }

    @Override
    public List<Location> getLocationByCategoryId(Long category_id) {
        return getJpaTemplate().find("select l from Location l where l.category.id=?1", category_id);
    }
}
