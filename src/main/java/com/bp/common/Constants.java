package com.bp.common;

/**
 * Created by Alifa on 3/14/2015.
 */
public final class Constants {
    public static final String ERROR_MESSAGE = "Fail";
    public static final Long ERROR_INDEX = 1L;
    public static final Long SUCCESS_INDEX = 0L;

    public static final String DEFAULT_SUCCESS = "OK";
    public static final String DEFAULT_FAIL = "FAIL";
}
