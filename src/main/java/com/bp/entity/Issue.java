package com.bp.entity;

/**
 * Created by Alifa on 6/22/2015.
 */
public class Issue {
    private String word;
    private Integer hits;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Issue(String word, Integer hits) {
        this.word = word;
        this.hits = hits;
    }
}
