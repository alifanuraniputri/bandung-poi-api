package com.bp.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Alifa on 3/27/2015.
 */
@Entity
@Table(name = "bp_check_in")
public class CheckIn {
    @TableGenerator(table = "tbl_sequence", name = "check_in_id_table",
            allocationSize = 100, initialValue = 0, pkColumnName = "seq_name",
            valueColumnName = "seq_count", pkColumnValue = "seq_check_in")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="check_in_id_table")
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "time_check_in")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar timeCheckIn;

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="location_id")
    private Location location;

    @Column(name = "status")
    private Integer status;

    @PrePersist
    protected void onCreate() {
        timeCheckIn = Calendar.getInstance();
        status = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getTimeCheckIn() {
        return timeCheckIn;
    }

    public void setTimeCheckIn(Calendar timeCheckIn) {
        this.timeCheckIn = timeCheckIn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
