package com.bp.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Alifa on 3/27/2015.
 */
@Entity
@Table(name = "bp_location")
public class Location {
    @TableGenerator(table = "tbl_sequence", name = "location_id_table",
            allocationSize = 100, initialValue = 0, pkColumnName = "seq_name",
            valueColumnName = "seq_count", pkColumnValue = "seq_location")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="location_id_table")
    @Id
    @Column(name = "id")
    private long id;

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="category_id")
    private Category category;

    @Column(name = "name", length = 64)
    private String name;

    @Column(name = "description", length = 255)
    private String description;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lng")
    private Double lng;

    @Column(name = "foursquare_id", length = 32)
    private String foursquareId;

    @Column(name = "popularity")
    private Double popularity;

    @Column(name = "rate")
    private Double rate;

    @Column(name = "criteria1")
    private Double criteria1;

    @Column(name = "criteria2")
    private Double criteria2;

    @Column(name = "criteria3")
    private Double criteria3;

    @Column(name = "criteria4")
    private Double criteria4;

    @Column(name = "criteria5")
    private Double criteria5;

    @Column(name = "issue1")
    private String issue1;

    @Column(name = "issue2")
    private String issue2;

    @Column(name = "issue3")
    private String issue3;

    @Column(name = "hits1")
    private Integer hits1;

    @Column(name = "hits2")
    private Integer hits2;

    @Column(name = "hits3")
    private Integer hits3;

    @Column(name = "status")
    private Integer status;

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar dateCreate;

    public Location() {
    }

    @PrePersist
    protected void onCreate() {
        dateCreate = Calendar.getInstance();
        status = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Calendar dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getCriteria1() {
        return criteria1;
    }

    public void setCriteria1(Double criteria1) {
        this.criteria1 = criteria1;
    }

    public Double getCriteria2() {
        return criteria2;
    }

    public void setCriteria2(Double criteria2) {
        this.criteria2 = criteria2;
    }

    public Double getCriteria3() {
        return criteria3;
    }

    public void setCriteria3(Double criteria3) {
        this.criteria3 = criteria3;
    }

    public Double getCriteria4() {
        return criteria4;
    }

    public void setCriteria4(Double criteria4) {
        this.criteria4 = criteria4;
    }

    public Double getCriteria5() {
        return criteria5;
    }

    public void setCriteria5(Double criteria5) {
        this.criteria5 = criteria5;
    }

    public String getIssue1() {
        return issue1;
    }

    public void setIssue1(String issue1) {
        this.issue1 = issue1;
    }

    public String getIssue2() {
        return issue2;
    }

    public void setIssue2(String issue2) {
        this.issue2 = issue2;
    }

    public String getIssue3() {
        return issue3;
    }

    public void setIssue3(String issue3) {
        this.issue3 = issue3;
    }

    public Integer getHits1() {
        return hits1;
    }

    public void setHits1(Integer hits1) {
        this.hits1 = hits1;
    }

    public Integer getHits2() {
        return hits2;
    }

    public void setHits2(Integer hits2) {
        this.hits2 = hits2;
    }

    public Integer getHits3() {
        return hits3;
    }

    public void setHits3(Integer hits3) {
        this.hits3 = hits3;
    }

    public String getFoursquareId() {
        return foursquareId;
    }

    public void setFoursquareId(String foursquareId) {
        this.foursquareId = foursquareId;
    }
}
