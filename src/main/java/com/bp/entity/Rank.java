package com.bp.entity;

/**
 * Created by Alifa on 6/19/2015.
 */
public class Rank {
    private Integer popularityRank;
    private Integer rateRank;
    private Integer total;

    public Integer getPopularityRank() {
        return popularityRank;
    }

    public void setPopularityRank(Integer popularityRank) {
        this.popularityRank = popularityRank;
    }

    public Integer getRateRank() {
        return rateRank;
    }

    public void setRateRank(Integer rateRank) {
        this.rateRank = rateRank;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
