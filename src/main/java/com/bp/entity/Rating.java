package com.bp.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Alifa on 3/27/2015.
 */

@Entity
@Table(name = "bp_rating")
public class Rating {
    @TableGenerator(table = "tbl_sequence", name = "rating_id_table",
            allocationSize = 100, initialValue = 0, pkColumnName = "seq_name",
            valueColumnName = "seq_count", pkColumnValue = "seq_rating")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="rating_id_table")
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar dateCreate;

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="user_id")
    private User user;

    @ManyToOne(cascade={CascadeType.MERGE})
    @JoinColumn(name="location_id")
    private Location location;

    @Column(name = "status")
    private Integer status;

    @Column(name = "overall")
    private Integer overall;

    @Column(name = "criteria1")
    private Integer criteria1;

    @Column(name = "criteria2")
    private Integer criteria2;

    @Column(name = "criteria3")
    private Integer criteria3;

    @Column(name = "criteria4")
    private Integer criteria4;

    @Column(name = "criteria5")
    private Integer criteria5;

    @PrePersist
    protected void onCreate() {
        dateCreate = Calendar.getInstance();
        status = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Calendar dateCreate) {
        this.dateCreate = dateCreate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getOverall() {
        return overall;
    }

    public void setOverall(Integer overall) {
        this.overall = overall;
    }

    public Integer getCriteria1() {
        return criteria1;
    }

    public void setCriteria1(Integer criteria1) {
        this.criteria1 = criteria1;
    }

    public Integer getCriteria2() {
        return criteria2;
    }

    public void setCriteria2(Integer criteria2) {
        this.criteria2 = criteria2;
    }

    public Integer getCriteria3() {
        return criteria3;
    }

    public void setCriteria3(Integer criteria3) {
        this.criteria3 = criteria3;
    }

    public Integer getCriteria4() {
        return criteria4;
    }

    public void setCriteria4(Integer criteria4) {
        this.criteria4 = criteria4;
    }

    public Integer getCriteria5() {
        return criteria5;
    }

    public void setCriteria5(Integer criteria5) {
        this.criteria5 = criteria5;
    }
}
