package com.bp.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by Alifa on 3/26/2015.
 */

@Entity
@Table(name = "bp_category")
public class Category {
    @TableGenerator(table = "tbl_sequence", name = "category_id_table",
            allocationSize = 100, initialValue = 0, pkColumnName = "seq_name",
            valueColumnName = "seq_count", pkColumnValue = "seq_category")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="category_id_table")
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "name", length = 64)
    private String name;

    @Column(name = "status")
    private Integer status;

    @Column(name = "description", length = 255)
    private String description;

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "MM")
    private Calendar dateCreate;

    @PrePersist
    protected void onCreate() {
        dateCreate = Calendar.getInstance();
        status = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Calendar dateCreate) {
        this.dateCreate = dateCreate;
    }
}
